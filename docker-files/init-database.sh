#!/bin/bash
set -e

echo "*** CREATING DATABASE, USERS AND ROLES ***"
#it initialize before starting the Postgres service
#for production grant the minimum privileges the application needs (select, insert, delete, update)
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
   CREATE USER darek WITH PASSWORD 'admin';
   CREATE DATABASE myprojectdevbridges;
   GRANT ALL PRIVILEGES ON DATABASE myprojectdevbridges TO darek;
   REVOKE ALL ON DATABASE myprojectdevbridges FROM PUBLIC;
   CREATE ROLE standarduser;
   CREATE USER appuser WITH PASSWORD 'user_password';
   \connect darek
   CREATE SCHEMA bridges;
   GRANT ALL PRIVILEGES ON SCHEMA bridges TO darek;
   GRAND CONNECT ON DATABASE myprojectdevbridges to standarduser;
   GRAND SELECT, UPDATE,INSERT,DELETE ON ALL TABLES IN SCHEMA bridges TO STANDARD USER
   GRANT standarduser TO appuser;
   GRANT USAGE ON SCHEMA bridges to standarduser;
EOSQL


ECHO "*** CREATING TABLE STRUCTURES ***"
psql -U admin admin < /docker-entrypoint-initdb.d/create-tables.sql
# CRM for bridges

## Project

Headless CRM and Rest api -  backend
Javascript technology - frondend (admin panel / client site)

## Requirements
- Java 11
- Docker

## Why this technology
There will be monolith first aproach.

I will be use monolith aproach, because deployment is much simpler. I create  all elemelets of the system on my own. There is no advandeges of use microservices for now. (the architecture may change)
there is no dedicated team on the app.

the goal is to create system that user can easly use


## How to run
**To build the application:**
1. to run backend
From the command line:

`$ cd bridges`
`$ mvnw clean install`
`$ mvnw spring-boot:run`


2. to run panel admin
not available at the moment

3. to run client site
not available at the moment

## How to work
Run local database from Docker images:
From the command line:
docker-compose up

## How to Contribute:
If you have interest in giving feedback or for participating to myBridge project in any way feel free to create new issue or open a PR (pull request).

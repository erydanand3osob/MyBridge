#System do zarzadzania mostem

features
- login
- logout
- change password

- save bridge
- read bridge
- update bridge
- delete bridge

- image serve
- upload images throw api
- custom error page

- pagination
- sortings

- google maps API

- frondend: react app or react native app
- react native
- aws serwer pipeline

Use Cases:

- Wyswietlanie mostu i jego danych
- Dodawanie mostów
- Edycja danych mostów
- Tworzenie protokołu online (dziennika objazdu)
- Wykazy porotokołów dla mostu
- Stworzenie protokołu online i zapis (3 rodzaje przeglądów)
- Wyświetlenie mostu na mapie i dostepnych protokołów
- Przypomnienie email o terminie pzregladu dróg
package pl.mybridges.bridges;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.mybridges.bridges.model.BridgeRecord;
import pl.mybridges.bridges.repository.BridgeRecordRepository;
import pl.mybridges.bridges.service.BridgeRecordServiceImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@DisplayName("Unit tests: handling service for bridge record class")
@Tag("unit")
class BridgeRecordServiceTest {
    @InjectMocks
    BridgeRecordServiceImpl bridgeRecordService;
    @Mock
    BridgeRecordRepository bridgeRecordRepository;

    @BeforeEach
    public void setup() {}

    @Test
    @DisplayName("should return single bridge record by id")
    void testFindById() throws Exception {
        // given
        BridgeRecord firstRecord =
                new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        ArgumentCaptor<Long> iDArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        // when
        bridgeRecordService.findBridgeById(firstRecord.getBridgeId());
        // then
        verify(bridgeRecordRepository, times(1)).findById(iDArgumentCaptor.capture());
        assertThat(iDArgumentCaptor.getValue()).isEqualTo(firstRecord.getBridgeId());
    }

    @Test
    @DisplayName("should return all bridge records")
    void testFindAll() {
        // given
        // when
        bridgeRecordService.findAll();
        // then
        verify(bridgeRecordRepository, times(1)).findAll();
    }

    @Test
    @DisplayName("save a bridge record by id is successful")
    void save() {
        BridgeRecord firstRecord =
                new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        ArgumentCaptor<BridgeRecord> bridgeRecordArgumentCaptor =
                ArgumentCaptor.forClass(BridgeRecord.class);

        given(bridgeRecordRepository.save(any(BridgeRecord.class))).willReturn(firstRecord);
        // when
        bridgeRecordService.save(firstRecord);
        // then
        verify(bridgeRecordRepository, times(1)).save(bridgeRecordArgumentCaptor.capture());
        assertThat(bridgeRecordArgumentCaptor.getValue()).isEqualTo(firstRecord);
    }

    @Test
    @DisplayName("delete bridge record by id is successful")
    void testDeleteById() {
        // given
        BridgeRecord firstRecord =
                new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        Long existingId = firstRecord.getBridgeId();
        ArgumentCaptor<Long> idArgumentCaptor = ArgumentCaptor.forClass(Long.class);
        // when
        bridgeRecordService.deleteById(existingId);
        // then
        verify(bridgeRecordRepository, times(1)).deleteById(idArgumentCaptor.capture());
        assertThat(idArgumentCaptor.getValue()).isEqualTo(existingId);
    }

    @Test
    @DisplayName("Update validated and corrected bridge record by id - saves all Values")
    void testUpdate() {
        // given
        BridgeRecord firstRecord =
                new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        given(bridgeRecordRepository.findById(anyLong())).willReturn(Optional.of(firstRecord));
        ArgumentCaptor<Long> idArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        Long existingId = firstRecord.getBridgeId();
        String diffName = "most Poniatowski";
        String diffCrosses = "rzeka Leonka";
        int diffYearOpened = 1975;
        String diffNotes = "the Old Bridge";

        BridgeRecord patchedBridgeRecord =
                new BridgeRecord(1L, diffName, diffCrosses, diffYearOpened, diffNotes);
        // when
        bridgeRecordService.update(firstRecord.getBridgeId(), patchedBridgeRecord);
        // then
        verify(bridgeRecordRepository, times(1)).findById(idArgumentCaptor.capture());

        assertThat(idArgumentCaptor.getValue()).isEqualTo(existingId);
        assertThat(firstRecord.getName()).isEqualTo(diffName);
        assertThat(firstRecord.getCrosses()).isEqualTo(diffCrosses);
        assertThat(firstRecord.getYearOpened()).isEqualTo(diffYearOpened);
        assertThat(firstRecord.getNotes()).isEqualTo(diffNotes);
    }

    @Test
    @DisplayName("Partial update validated and corrected bridge record by id - saves all Values")
    void testPartialUpdate() {
        // given
        BridgeRecord firstRecord =
                new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        given(bridgeRecordRepository.findById(anyLong())).willReturn(Optional.of(firstRecord));

        Long existingId = firstRecord.getBridgeId();
        String diffName = "the Goldengate bridge";
        Map<String, Object> patch = new HashMap<>();
        patch.put("name", diffName);

        ArgumentCaptor<BridgeRecord> bridgeRecordArgumentCaptor =
                ArgumentCaptor.forClass(BridgeRecord.class);
        ArgumentCaptor<Long> idArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        // when
        bridgeRecordService.partialUpdate(firstRecord.getBridgeId(), patch);
        // then

        verify(bridgeRecordRepository, times(1)).findById(idArgumentCaptor.capture());
        verify(bridgeRecordRepository, times(1)).save(bridgeRecordArgumentCaptor.capture());

        BridgeRecord capturedBridgeRecord = bridgeRecordArgumentCaptor.getValue();
        assertThat(idArgumentCaptor.getValue()).isEqualTo(existingId);
        assertThat(capturedBridgeRecord.getName()).isEqualTo(diffName);
    }
    // TODO: add by name , ba value , by Year,
    // TODO: Add sotings and pagination

}

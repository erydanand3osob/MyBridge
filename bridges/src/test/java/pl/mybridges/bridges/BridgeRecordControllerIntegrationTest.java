package pl.mybridges.bridges;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import pl.mybridges.bridges.controller.BridgeRecordController;
import pl.mybridges.bridges.exception.BridgeNotFoundException;
import pl.mybridges.bridges.exception.NullFieldValueException;
import pl.mybridges.bridges.model.BridgeRecord;
import pl.mybridges.bridges.service.BridgeRecordService;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*
 * Integration test: 1. Verifying http Request Matching 2. verifying Input Deserialization 3.
 * Verifying Input validation 4. Verifying Business Logic Calls 5. Verifying Output Serialization 6.
 * Verifying Exception Handling
 */
@DisplayName("Integration tests: Bridge Record Controller")
@Tag("Integration")
@AutoConfigureMockMvc(addFilters = false)
@WithMockUser(username = "user")
@WebMvcTest(BridgeRecordController.class)
class BridgeRecordControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;
    @MockBean
    BridgeRecordService bridgeRecords;

    private BridgeRecord firstRecord, secondRecord, thirdRecord;

    private final String URI = "/bridges";
    private List<BridgeRecord> records;

    @BeforeEach
    void setUp() {
        firstRecord = new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        secondRecord = new BridgeRecord(2L, "london Bridge", "bow river", 1995,
                "Bridge designed by MR Example");
        thirdRecord = new BridgeRecord(3L, "Iknow bridge", "empty road", 1964,
                "First full concrete bridge");
        records = new ArrayList<>(Arrays.asList(firstRecord, secondRecord, thirdRecord));
    }

    @DisplayName("Get all records (GET /bridges)")
    @Test
    public void getAllRecord_WithSuccess() throws Exception {

        Mockito.when(bridgeRecords.findAll()).thenReturn(records);

        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        List<BridgeRecord> record = mapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<BridgeRecord>>() {});

        verify(bridgeRecords, times(1)).findAll();
        assertThat(record).isNotNull();
        assertThat((record.stream().findFirst()).get().getBridgeId()).isEqualTo(1L);
        assertThat((record.stream().findFirst()).get().getName()).isEqualTo("most Pamaski");
        assertThat((record.stream().findFirst()).get().getCrosses()).isEqualTo("panama rzeka");
        assertThat((record.stream().findFirst()).get().getYearOpened()).isEqualTo(1895);
        assertThat((record.stream().findFirst()).get().getNotes()).isEqualTo("Pierwszy most");
    }

    @DisplayName("Check creation a one record (POST /bridges)")
    @Test
    public void createBridgeRecord_WithSuccess() throws Exception {
        BridgeRecord customRecord = BridgeRecord.builder().bridgeId(4L).name("The blue bridge")
                .crosses("Tamiza River").yearOpened(2050).notes("First bridge in Village").build();

        Mockito.when(bridgeRecords.save(any(BridgeRecord.class))).thenReturn(customRecord);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post(URI)
                .content(this.mapper.writeValueAsString(customRecord))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(mockRequest).andExpect(status().isCreated()).andReturn();

        String resultJson = result.getResponse().getContentAsString();

        verify(bridgeRecords, times(1)).save(any(BridgeRecord.class));
        assertThat(resultJson).isNotNull();
    }

    @DisplayName("Check one records (GET /bridges/{bridgeId})")
    @Test
    public void getBridgeByIdWithSuccess() throws Exception {

        Mockito.when(bridgeRecords.findBridgeById(firstRecord.getBridgeId()))
                .thenReturn(Optional.of(firstRecord));

        MvcResult result = mockMvc
                .perform(
                        MockMvcRequestBuilders.get(URI + "/" + firstRecord.getBridgeId().toString())
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        BridgeRecord record =
                mapper.readValue(result.getResponse().getContentAsString(), BridgeRecord.class);
        verify(bridgeRecords, times(1)).findBridgeById(firstRecord.getBridgeId());
        assertThat(record).isNotNull();
        assertThat(record.getBridgeId()).isEqualTo(firstRecord.getBridgeId());
        assertThat(record.getName()).isEqualTo(firstRecord.getName());
        assertThat(record.getYearOpened()).isEqualTo(firstRecord.getYearOpened());
        assertThat(record.getCrosses()).isEqualTo(firstRecord.getCrosses());
        assertThat(record.getNotes()).isEqualTo(firstRecord.getNotes());
    }

    @DisplayName("Check Raised Exceptions when record does not exist (GET /bridges/{bridgeId})")
    @Test
    public void getExceptionWhenGetBridgeRecordDoesNoExist() throws Exception {
        BridgeRecord customRecord =
                BridgeRecord.builder().bridgeId(6L).name("The red bridge").crosses("Red River")
                        .yearOpened(2023).notes("Second bridge in the Village").build();

        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.get(URI + "/" + customRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(customRecord));

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isNotFound()).andReturn();
        String resultJson = result.getResponse().getContentAsString();

        verify(bridgeRecords, times(0)).update(customRecord.getBridgeId(), customRecord);

        assertThat(result).isNotNull();
        assertThat(resultJson).isNotNull();
        assertFalse(result.getResolvedException() instanceof NullFieldValueException);
        assertTrue(result.getResolvedException() instanceof BridgeNotFoundException);
        assertEquals("Bridge with ID 6 not found",
                Objects.requireNonNull(result.getResolvedException()).getMessage());
    }

    @DisplayName("Check updating all values in a one records (PUT /bridges/{bridgeId})")
    @Test
    public void updateFullBridgeRecordWithSuccess() throws Exception {
        Optional<BridgeRecord> updatedBridgeRecord = Optional.ofNullable(
                BridgeRecord.builder().bridgeId(3L).name("Most Puławski").crosses("Wisła")
                        .yearOpened(2005).notes("Most wybudowany przez XYZ").build());

        Mockito.when(bridgeRecords.findBridgeById(thirdRecord.getBridgeId()))
                .thenReturn(Optional.of(thirdRecord));
        Mockito.when(bridgeRecords.update(updatedBridgeRecord.get().getBridgeId(), thirdRecord))
                .thenReturn(thirdRecord);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .put(URI + "/" + updatedBridgeRecord.get().getBridgeId().toString())
                .content(this.mapper.writeValueAsString(updatedBridgeRecord))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isNoContent()).andReturn();

        String resultJson = result.getResponse().getContentAsString();
        assertThat(resultJson).isNotNull();
    }

    @DisplayName("Check exception With Null in URL (PUT /bridges/{bridgeId})")
    @Test
    public void updateBridgeRecordWithNullInUrlShouldGetException() throws Exception {
        BridgeRecord existedBridgeRecord =
                BridgeRecord.builder().bridgeId(null).name("Most Puławski").crosses("Wisła")
                        .yearOpened(2005).notes("Most wybudowany przez XYZ").build();

        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.put(URI + "/" + existedBridgeRecord.getBridgeId())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(existedBridgeRecord));

        MvcResult result = mockMvc.perform(mockRequest).andExpect(status().isBadRequest())
                // .andExpect(jsonPath("$", is("Error while processing request")))
                .andReturn();

        verify(bridgeRecords, times(0)).update(existedBridgeRecord.getBridgeId(),
                existedBridgeRecord);

        assertThat(result).isNotNull();
        assertTrue(result.getResolvedException() instanceof MethodArgumentTypeMismatchException);
        assertEquals("Error while processing request",
                Objects.requireNonNull(result.getResponse().getContentAsString()));
    }

    @DisplayName("Check update  With Null in request body (PUT /bridges/{bridgeId})")
    @Test
    public void updateBridgeRecordWithNullInBridgeIdIsSuccess() throws Exception {
        BridgeRecord updateBridgeRecord = BridgeRecord.builder().name("Most Puławski")
                .crosses("Wisła").yearOpened(2005).notes("Most wybudowany przez XYZ").build();

        Mockito.when(bridgeRecords.findBridgeById(firstRecord.getBridgeId()))
                .thenReturn(Optional.ofNullable(firstRecord));
        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.put(URI + "/" + firstRecord.getBridgeId())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(updateBridgeRecord));

        MvcResult result = mockMvc.perform(mockRequest).andReturn();

        verify(bridgeRecords, times(1)).update(firstRecord.getBridgeId(), updateBridgeRecord);

        assertEquals(result.getResponse().getStatus(), HttpStatus.NO_CONTENT.value());
    }

    @DisplayName("Check Exception With Incomplete Request (PUT /bridges/{bridgeId})")
    @Test
    public void updateBridgeRecord_WithIncompleteRequest_isBadRequest() throws Exception {
        BridgeRecord customRecord =
                BridgeRecord.builder().bridgeId(4L).name("The blue bridge").build();

        Mockito.when(bridgeRecords.findBridgeById(customRecord.getBridgeId()))
                .thenReturn(Optional.of(customRecord));

        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.put(URI + "/" + customRecord.getBridgeId())
                        .content(this.mapper.writeValueAsString(customRecord))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isBadRequest()).andReturn();

        verify(bridgeRecords, times(0)).update(customRecord.getBridgeId(), customRecord);

        assertTrue(result.getResolvedException() instanceof NullFieldValueException);
        assertEquals("Bridge elements cannot be null (Try use PATCH REQUEST)",
                Objects.requireNonNull(result.getResolvedException()).getMessage());
    }

    @DisplayName("Check Exception record does not exists (PUT /bridges/{bridgeId})")
    @Test
    public void getExceptionWhenUpdatedBridgeRecordDoesNoExist() throws Exception {
        BridgeRecord customRecord =
                BridgeRecord.builder().bridgeId(7L).name("The red bridge").crosses("Red River")
                        .yearOpened(2023).notes("Second bridge in the Village").build();

        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.put(URI + "/" + customRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(customRecord));

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isNotFound()).andReturn();

        verify(bridgeRecords, times(0)).update(customRecord.getBridgeId(), customRecord);

        assertThat(result).isNotNull();
        assertTrue(result.getResolvedException() instanceof BridgeNotFoundException);
        assertEquals("Bridge with ID 7 not found",
                Objects.requireNonNull(result.getResolvedException()).getMessage());
    }

    @DisplayName("Check deleting one records (DELETE /bridges/{bridgeId})")
    @Test
    public void deleteRecordBridgeByIdWithSuccess() throws Exception {
        Mockito.when(bridgeRecords.findBridgeById(secondRecord.getBridgeId()))
                .thenReturn(Optional.of(secondRecord));

        mockMvc.perform(
                MockMvcRequestBuilders.delete(URI + "/" + secondRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(bridgeRecords, times(1)).deleteById(secondRecord.getBridgeId());
    }

    @DisplayName("Check Exception when recorddoes not exists (DELETE /bridges/{bridgeId})")
    @Test
    public void getExceptionWhenDeleteBridgeRecordWhichNotExist() throws Exception {
        BridgeRecord customRecord =
                BridgeRecord.builder().bridgeId(8L).name("The red bridge").crosses("Red River")
                        .yearOpened(2023).notes("Second bridge in the Village").build();

        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.delete(URI + "/" + customRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(customRecord));

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isNotFound()).andReturn();

        verify(bridgeRecords, times(0)).deleteById(customRecord.getBridgeId());

        assertTrue(result.getResolvedException() instanceof BridgeNotFoundException);
        assertEquals("Bridge with ID 8 not found",
                Objects.requireNonNull(result.getResolvedException()).getMessage());
    }

    @DisplayName("Check updating selected fields in one records (PATCH /bridges/{bridgeId}})")
    @Test
    public void partiallyUpdateBridgeRecord_WithSuccess() throws Exception {
        Map<String, Object> updatedBridgeRecord = new HashMap<>();
        updatedBridgeRecord.put("notes", "new notes");

        Mockito.when(bridgeRecords.findBridgeById(thirdRecord.getBridgeId()))
                .thenReturn(Optional.of(thirdRecord));

        MockHttpServletRequestBuilder mockRequest =
                MockMvcRequestBuilders.patch(URI + "/" + thirdRecord.getBridgeId().toString())
                        .content(this.mapper.writeValueAsString(updatedBridgeRecord))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isNoContent()).andReturn();

        String resultJson = result.getResponse().getContentAsString();

        assertThat(resultJson).isNotNull();
    }

    @DisplayName("Check Exception with Null in URL (PATCH /bridges/{bridgeId}})")
    @Test
    public void partiallyUpdateBridgeRecord_WithNullInUrl_ShouldGetException() throws Exception {
        Map<String, Object> updatedBridgeRecord = new HashMap<>();
        updatedBridgeRecord.put("notes", "new notes");

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.patch(URI + "/" + null)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(updatedBridgeRecord));

        MvcResult result =
                mockMvc.perform(mockRequest).andExpect(status().isBadRequest()).andReturn();

        String resultJson = result.getResponse().getContentAsString();
        assertThat(result).isNotNull();
        assertThat(resultJson).isNotNull();
        assertTrue(result.getResolvedException() instanceof MethodArgumentTypeMismatchException);
        assertEquals("Error while processing request",
                Objects.requireNonNull(result.getResponse().getContentAsString()));
    }
}

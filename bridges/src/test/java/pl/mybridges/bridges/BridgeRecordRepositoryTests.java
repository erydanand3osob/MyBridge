package pl.mybridges.bridges;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import pl.mybridges.bridges.repository.BridgeRecordRepository;

import static org.assertj.core.api.Assertions.assertThat;

// @ExtendWith(MockitoExtension.class)
@DataJpaTest
@TestPropertySource(properties = {"spring.jpa.hibernate.ddl-auto=create-drop"})
@Disabled
public class BridgeRecordRepositoryTests {

    @Autowired
    private BridgeRecordRepository bridgeRecordRepository;

    @Test
    void onlyBridgeRepositoryIsLoaded() {
        assertThat(bridgeRecordRepository).isNotNull();
        // other if existest should be null

    }
}
// @JpaTest
// BridgeRecordrepositry integration test

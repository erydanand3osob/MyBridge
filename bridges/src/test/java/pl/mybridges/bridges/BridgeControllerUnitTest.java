package pl.mybridges.bridges;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.mybridges.bridges.controller.BridgeRecordController;
import pl.mybridges.bridges.exception.BridgeNotFoundException;
import pl.mybridges.bridges.exception.BridgesExceptionAdvice;
import pl.mybridges.bridges.model.BridgeRecord;
import pl.mybridges.bridges.service.BridgeRecordServiceImpl;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Unit tests: handling bridgerecords")
@Tag("unit")
public class BridgeControllerUnitTest {

    private final ObjectMapper mapper = new ObjectMapper();
    private final String URI = "/bridges";
    @Mock
    BridgeRecordServiceImpl bridgeRecordService;
    @InjectMocks
    BridgeRecordController bridgeRecordController;
    private MockMvc mockMvc;
    private BridgeRecord firstRecord;
    private BridgeRecord secondRecord;

    @BeforeEach
    public void setup() {

        this.mockMvc = MockMvcBuilders.standaloneSetup(bridgeRecordController)
                .setControllerAdvice(new BridgesExceptionAdvice()).build();

        firstRecord = new BridgeRecord(1L, "most Pamaski", "panama rzeka", 1895, "Pierwszy most");
        secondRecord = new BridgeRecord(2L, "london Bridge", "bow river", 1995,
                "Bridge designed by MR Example");
    }

    @DisplayName("Check all records (GET /bridges)")
    @Test
    public void CanRetrieveAllWhenRecordsExists() throws Exception {
        // given
        Iterable<BridgeRecord> records = new ArrayList<>(Arrays.asList(firstRecord, secondRecord));

        given(bridgeRecordService.findAll()).willReturn(records);

        // when
        MockHttpServletResponse response =
                mockMvc.perform(MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON))
                        .andReturn().getResponse();

        // then
        List<BridgeRecord> responseRecords = mapper.readValue(response.getContentAsString(),
                new TypeReference<List<BridgeRecord>>() {});

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseRecords.toString()).isEqualTo(records.toString());
    }

    @DisplayName("Check one records (GET /bridges/{bridgeId})")
    @Test
    public void canRetrieveByIdWhenRecordExists() throws Exception {
        // given
        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willReturn(Optional.ofNullable(firstRecord));
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.get(URI + "/" + firstRecord.getBridgeId().toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        // then
        verify(bridgeRecordService, times(1)).findBridgeById(firstRecord.getBridgeId());

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(mapper.writeValueAsString(firstRecord));
    }

    @DisplayName("Check creation a one record (POST /bridges)")
    @Test
    public void CanCreateNewBridgeRecord() throws Exception {
        // given
        // first record
        // when
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.post(URI)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(firstRecord))).andReturn().getResponse();
        // then
        verify(bridgeRecordService, times(1)).save(any(BridgeRecord.class));
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @DisplayName("Check updating all values in a one records (PUT /bridges/{bridgeId})")
    @Test
    public void updateFullBridgeRecordWithSuccess() throws Exception {
        // given
        BridgeRecord updatedBridgeRecord = BridgeRecord.builder().bridgeId(1L).name("Most Puławski")
                .crosses("Wisła").yearOpened(2005).notes("Most wybudowany przez XYZ").build();

        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willReturn(Optional.of(firstRecord));

        // when
        MockHttpServletResponse response = mockMvc
                .perform(MockMvcRequestBuilders
                        .put(URI + "/" + firstRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(updatedBridgeRecord)))
                .andReturn().getResponse();
        // then
        assertEquals(response.getStatus(), HttpStatus.NO_CONTENT.value());
        verify(bridgeRecordService, times(1)).update(anyLong(), any(BridgeRecord.class));

        // TODO: add verification fpr captored values
    }

    @DisplayName("Check deleting one records (DELETE /bridges/{bridgeId})")
    @Test
    public void deleteRecordBridgeByIdWithSuccess() throws Exception {
        // given
        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willReturn(Optional.of(firstRecord));
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete(URI + "/" + firstRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        // then
        verify(bridgeRecordService, times(1)).deleteById(firstRecord.getBridgeId());
        assertEquals(response.getStatus(), HttpStatus.NO_CONTENT.value());
    }

    @DisplayName("Check updating selected fields in one records (PATCH /bridges/{bridgeId}})")
    @Test
    public void partiallyUpdateBridgeRecord_WithSuccess() throws Exception {
        // given
        Map<String, Object> updatedBridgeRecord = new HashMap<>();
        updatedBridgeRecord.put("notes", "new notes");
        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willReturn(Optional.of(firstRecord));
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.patch(URI + "/" + firstRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(updatedBridgeRecord))
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        // then
        verify(bridgeRecordService, times(1)).partialUpdate(anyLong(), eq(updatedBridgeRecord));
        assertEquals(response.getStatus(), HttpStatus.NO_CONTENT.value());
    }

    @Test
    @DisplayName("Check error getting when one record not exists(GET /bridges/{bridgeId}})")
    public void getExceptionWhenGetBridgeRecordDoesNoExist() throws Exception {
        // given
        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willThrow(new BridgeNotFoundException("Bridge with ID not found"));
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.get(URI + "/" + firstRecord.getBridgeId().toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.getContentAsString()).contains("Bridge with ID not found");
        verify(bridgeRecordService, never()).update(anyLong(), any(BridgeRecord.class));
    }

    @Test
    @DisplayName("Check deleting one records if record not exists (DELETE /bridges/{bridgeId}})")
    public void getExceptionWhenDeleteBridgeRecordWhichNotExist() throws Exception {
        // given
        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willThrow(new BridgeNotFoundException("Bridge with ID not found"));
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete(URI + "/" + firstRecord.getBridgeId().toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.getContentAsString()).contains("Bridge with ID not found");
        verify(bridgeRecordService, never()).deleteById(firstRecord.getBridgeId());
    }

    @Test
    @DisplayName("Check updating selected fields in one record when url is null value (PATCH /bridges/{bridgeId}})")
    public void partiallyUpdateBridgeRecord_WithNullInUrl_ShouldGetException() throws Exception {
        // given
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.patch(URI + "/" + null).accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON).content(""))
                .andReturn().getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        verify(bridgeRecordService, never()).partialUpdate(anyLong(), anyMap());
        // assertThat(response.getContentAsString()).contains("Error while processing
        // request");
    }

    @Test
    @DisplayName("Check updating a one records when url is null value (PUT /bridges/{bridgeId}})")
    public void updateBridgeRecordWithNullInUrlShouldGetException() throws Exception {
        // given
        // when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.put(URI + "/" + null).accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON).content(""))
                .andReturn().getResponse();
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        verify(bridgeRecordService, never()).partialUpdate(anyLong(), anyMap());
    }

    @Test
    @DisplayName("Check updating selected fields in one records (PUT /bridges/{bridgeId}})")
    public void updateBridgeRecord_WithIncompleteRequest_isBadRequest() throws Exception {
        // given
        BridgeRecord updatedBridgeRecord = BridgeRecord.builder().bridgeId(1L)
                // missing fields
                .yearOpened(2005).notes("Most wybudowany przez XYZ").build();

        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willReturn(Optional.of(firstRecord));

        // when
        MockHttpServletResponse response = mockMvc
                .perform(MockMvcRequestBuilders
                        .put(URI + "/" + firstRecord.getBridgeId().toString())
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(updatedBridgeRecord)))
                .andReturn().getResponse();
        // then
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
        verify(bridgeRecordService, never()).update(anyLong(), any(BridgeRecord.class));
    }

    @Test
    @DisplayName("Check updating when a record record not exists (PATCH /bridges/{bridgeId}})")
    public void getExceptionWhenUpdatedBridgeRecordDoesNoExist() throws Exception {
        // given
        given(bridgeRecordService.findBridgeById(firstRecord.getBridgeId()))
                .willThrow(new BridgeNotFoundException("Bridge with ID not found"));
        // when
        MockHttpServletResponse response = mockMvc
                .perform(MockMvcRequestBuilders
                        .put(URI + "/" + firstRecord.getBridgeId().toString())
                        .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON)
                        .content(this.mapper.writeValueAsString(firstRecord)))
                .andReturn().getResponse();
        // then
        verify(bridgeRecordService, never()).update(firstRecord.getBridgeId(), firstRecord);
        assertThat(response.getContentAsString()).contains("Bridge with ID not found");
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }
}

package pl.mybridges.bridges;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import pl.mybridges.bridges.exception.BridgeNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Unit tests: exception classes")
@Tag("unit")
class BridgeNotFoundExceptionTest {

    @Test
    @DisplayName("test BridgeNotFound class")
    void shouldThrowBridgeNotFoundException() {
        Throwable exception = assertThrows(BridgeNotFoundException.class, () -> {
            throw new BridgeNotFoundException("bridge not found");
        });
        assertEquals("bridge not found", exception.getMessage());
    }
}

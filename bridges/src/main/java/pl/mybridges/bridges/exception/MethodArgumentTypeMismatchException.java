package pl.mybridges.bridges.exception;

public class MethodArgumentTypeMismatchException extends RuntimeException {
    MethodArgumentTypeMismatchException(String message) {
        super(message);
    };
}

package pl.mybridges.bridges.exception;

public class BridgeNotFoundException extends RuntimeException {
    public BridgeNotFoundException(String message) {
        super(message);
    }
}

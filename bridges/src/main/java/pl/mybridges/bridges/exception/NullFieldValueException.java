package pl.mybridges.bridges.exception;

public class NullFieldValueException extends RuntimeException {

    public NullFieldValueException(String message) {
        super(message);
    }
}

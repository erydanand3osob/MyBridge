package pl.mybridges.bridges.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mybridges.bridges.model.BridgeRecord;

@Repository
public interface BridgeRecordRepository extends JpaRepository<BridgeRecord, Long> {
}

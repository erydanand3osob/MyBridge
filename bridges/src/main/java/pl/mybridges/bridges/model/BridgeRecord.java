package pl.mybridges.bridges.model;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bridge_record")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class BridgeRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bridge_id")
    private Long bridgeId;

    @Column(name = "bridge_name")
    private String name;

    @Column(name = "crosses")
    private String crosses;

    @Column(name = "year_opened")
    private Integer yearOpened;

    @Column(name = "notes")
    private String notes;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BridgeRecord that = (BridgeRecord) o;
        return name.equals(that.name) && crosses.equals(that.crosses)
                && yearOpened.equals(that.yearOpened);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, crosses, yearOpened);
    }
}

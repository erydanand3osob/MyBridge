package pl.mybridges.bridges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;

@SpringBootApplication
public class BridgesApplication {

    public static void main(String[] args) {
        SpringApplication.run(BridgesApplication.class, args);
    }
}

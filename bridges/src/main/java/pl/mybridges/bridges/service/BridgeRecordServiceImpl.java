package pl.mybridges.bridges.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;
import pl.mybridges.bridges.model.BridgeRecord;
import pl.mybridges.bridges.repository.BridgeRecordRepository;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class BridgeRecordServiceImpl implements BridgeRecordService {

    private final BridgeRecordRepository bridgeRecordRepository;

    @Override
    public Optional<BridgeRecord> findBridgeById(Long id) {
        return bridgeRecordRepository.findById(id);
    }

    @Override
    public Iterable<BridgeRecord> findAll() {
        return bridgeRecordRepository.findAll();
    }


    @Override
    public BridgeRecord save(BridgeRecord bridgeRecord) {
        return bridgeRecordRepository.save(bridgeRecord);
    }

    @Override
    public void deleteById(Long id) {
        bridgeRecordRepository.deleteById(id);
    }

    @Transactional
    @Override
    public BridgeRecord update(Long bridgeId, BridgeRecord newBridgeRecord) {
        Optional<BridgeRecord> optionalBridgeRecord =
                Optional.of(findBridgeById(bridgeId)).orElseThrow();

        optionalBridgeRecord.get().setName(newBridgeRecord.getName());
        optionalBridgeRecord.get().setCrosses(newBridgeRecord.getCrosses());
        optionalBridgeRecord.get().setYearOpened(newBridgeRecord.getYearOpened());
        optionalBridgeRecord.get().setNotes(newBridgeRecord.getNotes());
        return optionalBridgeRecord.get();
    }

    @Transactional
    @Override
    public BridgeRecord partialUpdate(Long bridgeId, Map<String, Object> newFields) {
        Optional<BridgeRecord> bridgeRecord = findBridgeById(bridgeId);
        newFields.forEach((key, value) -> {
            Field field = ReflectionUtils.findField(BridgeRecord.class, key);
            if (field != null) {
                field.setAccessible(true);
                ReflectionUtils.setField(field, bridgeRecord.get(), value);
            }
        });
        return bridgeRecordRepository.save(bridgeRecord.get());
    }
}

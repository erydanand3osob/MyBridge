package pl.mybridges.bridges.service;

import pl.mybridges.bridges.model.BridgeRecord;

import java.util.Map;
import java.util.Optional;

public interface BridgeRecordService {
    Optional<BridgeRecord> findBridgeById(Long id);

    Iterable<BridgeRecord> findAll();

    BridgeRecord save(BridgeRecord bridgeRecord);

    void deleteById(Long id);

    BridgeRecord update(Long bridgeId, BridgeRecord existingBridgeRecord);

    BridgeRecord partialUpdate(Long bridgeId, Map<String, Object> patchBridgeRecord);
}

package pl.mybridges.bridges.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mybridges.bridges.exception.BridgeNotFoundException;
import pl.mybridges.bridges.exception.NullFieldValueException;
import pl.mybridges.bridges.model.BridgeRecord;
import pl.mybridges.bridges.service.BridgeRecordService;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/bridges")
public class BridgeRecordController {

    private final BridgeRecordService bridgeRecordService;

    @GetMapping
    public Iterable<BridgeRecord> getAllRecords() {
        return bridgeRecordService.findAll();
    }

    @PostMapping
    public ResponseEntity<BridgeRecord> createBridgeRecord(
            @RequestBody @Valid BridgeRecord bridgeRecord) throws NullFieldValueException {
        if (bridgeRecord.getBridgeId() == null) {
            throw new NullFieldValueException("ID value is null, It create new Record instead");
        }
        BridgeRecord newBridgeRecord = bridgeRecordService.save(bridgeRecord);
        return new ResponseEntity<>(newBridgeRecord, HttpStatus.CREATED);
    }

    @GetMapping("/{bridgeId}")
    public Optional<BridgeRecord> getBridgeById(@PathVariable(value = "bridgeId") Long bridgeId) {
        return Optional.ofNullable(bridgeRecordService.findBridgeById(bridgeId)
                .orElseThrow(() -> new BridgeNotFoundException(
                        "Bridge with ID " + bridgeId.toString() + " not found")));
    }

    @PutMapping("/{bridgeId}")
    public ResponseEntity<BridgeRecord> updateBridgeRecord(
            @RequestBody BridgeRecord newBridgeRecord,
            @PathVariable(value = "bridgeId") Long bridgeId)
            throws BridgeNotFoundException, NullFieldValueException {

        Optional<BridgeRecord> optionalBridgeRecord = bridgeRecordService.findBridgeById(bridgeId);
        if (optionalBridgeRecord.isEmpty()) {
            throw new BridgeNotFoundException("Bridge with ID " + bridgeId + " not found");
        }
        if ((newBridgeRecord.getName() == null) || (newBridgeRecord.getCrosses() == null)
                || (newBridgeRecord.getNotes() == null)
                || (newBridgeRecord.getYearOpened() == null)) {
            throw new NullFieldValueException(
                    "Bridge elements cannot be null (Try use PATCH REQUEST)");
        }
        // bridge id can be null - we can process request with id provided from url
        if (!(newBridgeRecord.getBridgeId() == null)) {
            if (!(newBridgeRecord.getBridgeId().equals(bridgeId))) {
                throw new BridgeNotFoundException("Bridge with ID " + bridgeId
                        + " not match with id provided in request // Id in PUT request is redundant");
            }
        }

        BridgeRecord updatedBridgeRecord = bridgeRecordService.update(bridgeId, newBridgeRecord);

        return new ResponseEntity<>(updatedBridgeRecord, HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{bridgeId}")
    public ResponseEntity<BridgeRecord> partiallyUpdateBridge(
            @RequestBody Map<String, Object> patchedBridgeRecord,
            @PathVariable(value = "bridgeId") Long bridgeId) throws BridgeNotFoundException {
        Optional<BridgeRecord> optionalBridgeRecord = bridgeRecordService.findBridgeById(bridgeId);
        if (optionalBridgeRecord.isEmpty()) {
            throw new BridgeNotFoundException("Bridge with ID " + bridgeId + " not found");
        }

        BridgeRecord updatedBridgeRecord =
                bridgeRecordService.partialUpdate(bridgeId, patchedBridgeRecord);

        return new ResponseEntity<>(updatedBridgeRecord, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{bridgeId}")
    public ResponseEntity<Void> deleteBridgeById(@PathVariable(value = "bridgeId") Long bridgeId)
            throws BridgeNotFoundException {
        if (bridgeRecordService.findBridgeById(bridgeId).isEmpty()) {
            throw new BridgeNotFoundException("Bridge with ID " + bridgeId + " not found");
        }
        bridgeRecordService.deleteById(bridgeId);
        return ResponseEntity.noContent().build();
    }
}
